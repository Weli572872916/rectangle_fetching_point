package com.example.fail5.myapplication.utils;


import java.util.Comparator;

/**
 * @author Administrator
 * @name CarControl
 * @class name：com.example.administrator.carcontrol.utils
 * @class describe
 * @time 2017-10-18 18:13
 * @change
 * @chang time
 * @class describe
 */
public class ScaleUtlis {
    public static String intToHex(int n) {
        char[] ch = new char[20];
        int nIndex = 0;
        while (true) {
            int m = n / 16;
            int k = n % 16;
            if (k == 15) {
                ch[nIndex] = 'F';
            } else if (k == 14) {
                ch[nIndex] = 'E';
            } else if (k == 13) {
                ch[nIndex] = 'D';
            } else if (k == 12) {
                ch[nIndex] = 'C';
            } else if (k == 11) {
                ch[nIndex] = 'B';
            } else if (k == 10) {
                ch[nIndex] = 'A';
            } else {
                ch[nIndex] = (char) ('0' + k);
                nIndex++;
                if (m == 0) {
                    break;
                }
                n = m;
            }
        }
        StringBuilder sb = new StringBuilder();
        sb.append(ch, 0, nIndex);
        sb.reverse();
        String strHex = new String("00");
        strHex += sb.toString();
        return strHex;
    }

    public static class MyComparator implements Comparator<Integer> {
        @Override
        public int compare(Integer o1, Integer o2) {
            /**从小到大*    o2-o1则为从大到小*/
            return o1 - o2;
        }
    }




    /**
     * 倒叙
     */
    public static class MyComparatorReverseOrder implements Comparator<Integer> {
        @Override
        public int compare(Integer o1, Integer o2) {
            /**从小到大*    o2-o1则为从大到小*/
            return o2 - o1;
        }
    }
}
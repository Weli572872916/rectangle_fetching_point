package com.example.fail5.myapplication.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.fail5.myapplication.R;
import com.example.fail5.myapplication.db.DbUtil;
import com.example.fail5.myapplication.db.DeskDetailHelper;
import com.example.fail5.myapplication.entity.DeskDetail;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * @author Administrator
 */
public class PathDialogFragment extends DialogFragment {
    private static final String ARG_PARAM1 = "type";
    private static final String ARG_PARAM2 = "row";
    private static final String ARG_PARAM3 = "carPoint";
    private static final String ARG_PARAM4 = "integerSet";
    @BindView(R.id.ed_desk_input)
    EditText edDeskInput;

    @BindView(R.id.bt_desk_clear)
    Button btDeskClear;
    @BindView(R.id.bt_desk_ok)
    Button btDeskOk;
    Unbinder unbinder;
    Point carPoint;
    int row = -99;
    int type = -99;
    private DeskDetailHelper mHelper;
    public static final String REQUESE = "RESOIBSE";

    public PathDialogFragment() {
    }

    public static PathDialogFragment newInstance(int param1, int param2, Point c, ArrayList<Integer> integerSet) {
        PathDialogFragment fragment = new PathDialogFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        args.putInt(ARG_PARAM2, param2);
        args.putParcelable(ARG_PARAM3, c);
        args.putIntegerArrayList(ARG_PARAM4, integerSet);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams params = window != null ? window.getAttributes() : null;
        params.gravity = Gravity.CENTER;
        params.width = WindowManager.LayoutParams.WRAP_CONTENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        assert window != null;
        window.setAttributes(params);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getDialog().setCanceledOnTouchOutside(false);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_desk_set, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHelper = DbUtil.getDriverHelper();
        if (getArguments() != null) {
            row = getArguments().getInt(ARG_PARAM2);
            type = getArguments().getInt(ARG_PARAM1);
            carPoint = getArguments().getParcelable(ARG_PARAM3);

        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void addData(String name, Point point) {
        DeskDetail deskDetail = new DeskDetail();
        deskDetail.setDeskRow(row);
        deskDetail.setDeskName(name);
        deskDetail.setDeskType(type);
        deskDetail.setXValue(point.x);
        deskDetail.setYValue(point.y);
        mHelper.save(deskDetail);


        setData();
    }


    /**
     * 返回DeskFragment
     */
    protected void setData() {
        if (getTargetFragment() == null) {
            return;
        }
        Intent intent = new Intent();
        //获得目标Fragment,并将数据通过onActivityResult放入到intent中进行传值
        getTargetFragment().onActivityResult(DeskFragment.RESUSET_CODE, Activity.RESULT_OK, intent);

        dismiss();
    }


    @OnClick({R.id.bt_desk_clear, R.id.bt_desk_ok})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.bt_desk_clear:
                dismiss();
                break;
            case R.id.bt_desk_ok:
                String name = edDeskInput.getText().toString().trim();

                if (row == 99 || type == 99) {
                    Toast.makeText(getActivity(), "数据错误,不能添加", Toast.LENGTH_SHORT).show();
                }
                if (carPoint == null) {
                    Toast.makeText(getActivity(), "未获取到点", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(name)) {
                    Toast.makeText(getActivity(), "桌台名字为空", Toast.LENGTH_SHORT).show();


                } else {

                    addData(name,  carPoint);
                }
                break;
        }
    }
}

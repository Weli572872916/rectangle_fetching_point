package com.example.fail5.myapplication.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Switch;
import android.widget.Toast;


import com.example.fail5.myapplication.PathMap;
import com.example.fail5.myapplication.R;
import com.example.fail5.myapplication.db.DbUtil;
import com.example.fail5.myapplication.db.DeskDetailHelper;
import com.example.fail5.myapplication.entity.DeskDetail;
import com.example.fail5.myapplication.utils.DensityUtils;
import com.qmuiteam.qmui.widget.dialog.QMUIDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * @author Administrator
 * @name CarControl
 * @class name：com.example.administrator.carcontrol.fragment
 * @class describe
 * @time 2017/9/27 14:43
 * @change
 * @chang time
 * @class describe
 */
public class DeskFragment extends Fragment  {


    Unbinder unbinder;



    private DeskDetailHelper mHelper;
    protected static final int RESUSET_CODE = 0;
    private List<DeskDetail> deskDetails;

    @BindView(R.id.s_v)
    Switch sV;
    @BindView(R.id.path_view)
    PathMap pathView;
    public int addCount = 0;






    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }




    public static DeskFragment newInstance() {
        DeskFragment fragment = new DeskFragment();
        return fragment;
    }




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_desk_show, container, false);
        unbinder = ButterKnife.bind(this, view);


        // 方法2
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        float width = dm.widthPixels * dm.density;
        float height = dm.heightPixels * dm.density;
        Log.d("DeskFragment", "width:" + width);
        Log.d("DeskFragment", "height:" + height);
        //屏幕分辨率容器
        DisplayMetrics mDisplayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);
        float density = mDisplayMetrics.density;
        int width1 = mDisplayMetrics.widthPixels;
        int height1 = mDisplayMetrics.heightPixels;

        int densityDpi = mDisplayMetrics.densityDpi;

        Log.d("DeskFragment", "Screen Ratio: [" + width1 + "x" + height1 + "],density=" + density + ",densityDpi=" + densityDpi);
        Log.d("DeskFragment", "Screen mDisplayMetrics: " + mDisplayMetrics);
        iniData();
        initView();
        return view;
    }

    /**
     * @author Weli
     * @time 2017/9/30  10:34
     * @describe
     */
    private void iniData() {
        deskDetails = new ArrayList<>();

        mHelper = DbUtil.getDriverHelper();
        getData();
        float a1 = (float) 87;
        float b1 = (float) 607;
        Log.d("DeskFragment", "DensityUtils.px2dp(a):" +DensityUtils.px2dip(getActivity(),a1)) ;
        Log.d("DeskFragment", "DensityUtils.px2dp(a):" + DensityUtils.px2dip(getActivity(),b1));




        float a = (float) 66;
        float b = (float) 462;
        Log.d("DeskFragment", "DensityUtils.px2dp(a):" +DensityUtils.dip2px(getActivity(),a)) ;
        Log.d("DeskFragment", "DensityUtils.px2dp(a):" +DensityUtils.dip2px(getActivity(),b));



    }

    public void getData() {
        deskDetails.clear();
        deskDetails = mHelper.queryAll();
        Log.d("DeskFragment", "deskDetails:" + deskDetails);
        if (deskDetails.size() > 0) {
            pathView.refreshPoint(deskDetails);
        }

    }

    /**
     * 初始化组件
     */
    private void initView() {

        pathView.setOnViewClick(new PathMap.OnViewClick() {
            @Override
            public void onClick(final Point point, final int row, final ArrayList<Integer> integerSet) {
                String[] strings = new String[]{"读取点", "补点"};
                new QMUIDialog.MenuDialogBuilder(getActivity())
                        .setActionDivider(2, R.color.color5, 0, strings.length)
                        .addItems(strings, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getScreenSizeOfDevice2();
                                Log.d("DeskFragment", "point:" + point);
                                point.set((int) DensityUtils.px2dp(getActivity(), point.x), (int) DensityUtils.px2dp(getActivity(), point.y));
                                Log.d("DeskFragment", "point:" + point);
                                intent2PathDialog(which, row, point, integerSet);
                                dialog.dismiss();
                            }
                        }).show();
            }





            @Override
            public void onLongClickListener(Point point) {
                Log.d("DeskViewkFragment", "修改");
            }
        });
        sV.setChecked(false);
        sV.setSwitchTextAppearance(
                getActivity(), R.style.s_false);
        sV.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()

        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                //控制开关字体颜色
                if (b) {
                    sV.setSwitchTextAppearance(getActivity(), R.style.s_true);
                    pathView.change(true);

                } else {
                    sV.setSwitchTextAppearance(getActivity(), R.style.s_false);
                    pathView.change(false);
                }
            }
        });
    }



    @Override
    public void onHiddenChanged(boolean hidden) {
        if (!hidden) {
//            deskClear();
//            iniData();
        }
        super.onHiddenChanged(hidden);

    }



    /**
     * 接收返回参数
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESUSET_CODE) {
            String stringExtra = data.getStringExtra(PathDialogFragment.REQUESE);

            getData();
        }
    }




    /**
     * 调转到到PathDialogFragment
     *
     * @param type
     * @param row
     * @param point
     * @param integerSet
     */
    private void intent2PathDialog(int type, int row, Point point, ArrayList<Integer> integerSet) {
        PathDialogFragment fragment = PathDialogFragment.newInstance(type, row, point, integerSet);
        //设置目标Fragment
        fragment.setTargetFragment(DeskFragment.this, RESUSET_CODE);
        assert getFragmentManager() != null;
        fragment.show(getFragmentManager(), PathDialogFragment.class.getSimpleName());
    }

    private void getScreenSizeOfDevice2() {
        Point point = new Point();
        getActivity().getWindowManager().getDefaultDisplay().getRealSize(point);
        DisplayMetrics dm = getResources().getDisplayMetrics();
        double x = Math.pow(point.x / dm.xdpi, 2);
        double y = Math.pow(point.y / dm.ydpi, 2);
        double screenInches = Math.sqrt(x + y);
        Log.d("22222222222", "Screen inches : " + screenInches);
    }



}

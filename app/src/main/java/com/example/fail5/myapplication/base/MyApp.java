package com.example.fail5.myapplication.base;

import android.app.Application;
import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;

import com.example.fail5.myapplication.db.DbCore;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * @author Weli
 * @time 2017-10-16  16:57
 * @describe Application
 */
public class MyApp extends Application {
    public static List<?> images = new ArrayList<>();

    public static int H, W;
    public static MyApp app;
    private static Context mContext;
    @Override
    public void onCreate() {
        // 应用程序入口处调用，避免手机内存过小，杀死后台进程后通过历史intent进入Activity造成SpeechUtility对象为null
        // 如在Application中调用初始化，需要在Mainifest中注册该Applicaiton
        // 注意：此接口在非主进程调用会返回null对象，如需在非主进程使用语音功能，请增加参数：SpeechConstant.FORCE_LOGIN+"=true"
        // 参数间使用半角“,”分隔。
        // 设置你申请的应用appid,请勿在'='与appid之间添加空格及空转义符
        // 注意： appid 必须和下载的SDK保持一致，否则会出现10407错误
        super.onCreate();

        // 以下语句用于设置日志开关（默认开启），设置成false时关闭语音云SDK日志打印
        /*  Setting.setShowLog(false);*/
        Log.d("MyApp", "wo");

        app = this;

        DbCore.init(this);
        DbCore.enableQueryBuilderLog(); //开启调试 log
        mContext = this;

        String[] urls = {"file://" + Environment.getExternalStorageDirectory().getPath() + "/test.jpg"};
        List list = Arrays.asList(urls);
        images = new ArrayList(list);

        Context context = getApplicationContext();
        // 获取当前包名
        String packageName = context.getPackageName();
        // 获取当前进程名
        String processName = getProcessName(android.os.Process.myPid());


    }

    /**
     * 获取进程号对应的进程名
     *
     * @param pid 进程号
     * @return 进程名
     */
    private static String getProcessName(int pid) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("/proc/" + pid + "/cmdline"));
            String processName = reader.readLine();
            if (!TextUtils.isEmpty(processName)) {
                processName = processName.trim();
            }
            return processName;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
        return null;
    }

    public void getScreen(Context aty) {
        DisplayMetrics dm = aty.getResources().getDisplayMetrics();
        H = dm.heightPixels;
        W = dm.widthPixels;
    }

    /**
     * 获取全局上下文
     *
     * @return Application context
     */
    public static Context getContext() {
        return mContext;
    }
}

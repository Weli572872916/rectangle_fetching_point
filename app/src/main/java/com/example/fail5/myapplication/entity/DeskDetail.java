package com.example.fail5.myapplication.entity;


import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class DeskDetail {
    @Id(autoincrement = true) // id自增长
    private Long id;
    private String createDate;
    private int deskCol;
    private String deskDescribe;
    private String deskLocation;
    private String deskName;
    private int deskRow;
    private String deskStatus;
    private int deskType;
    private int deskId;
    private String instruct;
    private int xValue;
    private int yValue;
    @Generated(hash = 1798807616)
    public DeskDetail(Long id, String createDate, int deskCol, String deskDescribe,
                      String deskLocation, String deskName, int deskRow, String deskStatus,
                      int deskType, int deskId, String instruct, int xValue, int yValue) {
        this.id = id;
        this.createDate = createDate;
        this.deskCol = deskCol;
        this.deskDescribe = deskDescribe;
        this.deskLocation = deskLocation;
        this.deskName = deskName;
        this.deskRow = deskRow;
        this.deskStatus = deskStatus;
        this.deskType = deskType;
        this.deskId = deskId;
        this.instruct = instruct;
        this.xValue = xValue;
        this.yValue = yValue;
    }
    @Generated(hash = 2097238377)
    public DeskDetail() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getCreateDate() {
        return this.createDate;
    }
    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
    public int getDeskCol() {
        return this.deskCol;
    }
    public void setDeskCol(int deskCol) {
        this.deskCol = deskCol;
    }
    public String getDeskDescribe() {
        return this.deskDescribe;
    }
    public void setDeskDescribe(String deskDescribe) {
        this.deskDescribe = deskDescribe;
    }
    public String getDeskLocation() {
        return this.deskLocation;
    }
    public void setDeskLocation(String deskLocation) {
        this.deskLocation = deskLocation;
    }
    public String getDeskName() {
        return this.deskName;
    }
    public void setDeskName(String deskName) {
        this.deskName = deskName;
    }
    public int getDeskRow() {
        return this.deskRow;
    }
    public void setDeskRow(int deskRow) {
        this.deskRow = deskRow;
    }
    public String getDeskStatus() {
        return this.deskStatus;
    }
    public void setDeskStatus(String deskStatus) {
        this.deskStatus = deskStatus;
    }
    public int getDeskType() {
        return this.deskType;
    }
    public void setDeskType(int deskType) {
        this.deskType = deskType;
    }
    public int getDeskId() {
        return this.deskId;
    }
    public void setDeskId(int deskId) {
        this.deskId = deskId;
    }
    public String getInstruct() {
        return this.instruct;
    }
    public void setInstruct(String instruct) {
        this.instruct = instruct;
    }
    public int getXValue() {
        return this.xValue;
    }
    public void setXValue(int xValue) {
        this.xValue = xValue;
    }
    public int getYValue() {
        return this.yValue;
    }
    public void setYValue(int yValue) {
        this.yValue = yValue;
    }

    @Override
    public String toString() {
        return "DeskDetail{" +
                "id=" + id +
                ", createDate='" + createDate + '\'' +
                ", deskCol=" + deskCol +
                ", deskDescribe='" + deskDescribe + '\'' +
                ", deskLocation='" + deskLocation + '\'' +
                ", deskName='" + deskName + '\'' +
                ", deskRow=" + deskRow +
                ", deskStatus='" + deskStatus + '\'' +
                ", deskType=" + deskType +
                ", deskId=" + deskId +
                ", instruct='" + instruct + '\'' +
                ", xValue=" + xValue +
                ", yValue=" + yValue +
                '}';
    }




}
